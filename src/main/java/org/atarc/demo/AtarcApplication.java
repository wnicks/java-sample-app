package org.atarc.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class AtarcApplication {

	@GetMapping("/")
	String home() {
		return "<html><P><P><div align=center> <H1>Welcome to yourGitDemo! May 17, the Luckiest day!!!</h1></div> <br><br> LUCKY!!! </html>";
		//return "<html><P><P><div align=center> <H1>Welcome to ATARC!</h1></div> </html>";
	}

	public static void main(String[] args) {
		SpringApplication.run(AtarcApplication.class, args);
	}
}
